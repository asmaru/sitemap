<?php

namespace asmaru\sitemap;

use PHPUnit\Framework\TestCase;

class SiteMapBuilderTest extends TestCase {

	public function testCanCreateEmptyXml(): void {
		$sitemap = new SiteMapBuilder();
		$xml = $sitemap->__toString();
		$expected = <<<'TAG'
<?xml version="1.0"?>
<urlset xmlns="http://www.sitemaps.org/schemas/sitemap/0.9"/>

TAG;
		$this->assertEquals($expected, $xml);
	}

	public function testCanCreateItem() {
		$sitemap = new SiteMapBuilder();
		$sitemap->add('http://example.org/example1.html', 0, Frequency::always(), 1);
		$sitemap->add('http://example.org/example2.html', 0, Frequency::hourly(), 0.8);
		$sitemap->add('http://example.org/example3.html', 0, Frequency::daily(), 0.6);
		$sitemap->add('http://example.org/example4.html', 0, Frequency::weekly(), 0.4);
		$sitemap->add('http://example.org/example5.html', 0, Frequency::monthly(), 0.2);
		$sitemap->add('http://example.org/example6.html', 0, Frequency::yearly(), 0);
		$sitemap->add('http://example.org/example7.html', 0, Frequency::never());
		$xml = $sitemap->__toString();

		$expected = <<<'TAG'
<?xml version="1.0"?>
<urlset xmlns="http://www.sitemaps.org/schemas/sitemap/0.9">
  <url>
    <loc>http://example.org/example1.html</loc>
    <lastmod>1970-01-01T00:00:00+00:00</lastmod>
    <changefreq>always</changefreq>
    <priority>1</priority>
  </url>
  <url>
    <loc>http://example.org/example2.html</loc>
    <lastmod>1970-01-01T00:00:00+00:00</lastmod>
    <changefreq>hourly</changefreq>
    <priority>0.8</priority>
  </url>
  <url>
    <loc>http://example.org/example3.html</loc>
    <lastmod>1970-01-01T00:00:00+00:00</lastmod>
    <changefreq>daily</changefreq>
    <priority>0.6</priority>
  </url>
  <url>
    <loc>http://example.org/example4.html</loc>
    <lastmod>1970-01-01T00:00:00+00:00</lastmod>
    <changefreq>weekly</changefreq>
    <priority>0.4</priority>
  </url>
  <url>
    <loc>http://example.org/example5.html</loc>
    <lastmod>1970-01-01T00:00:00+00:00</lastmod>
    <changefreq>monthly</changefreq>
    <priority>0.2</priority>
  </url>
  <url>
    <loc>http://example.org/example6.html</loc>
    <lastmod>1970-01-01T00:00:00+00:00</lastmod>
    <changefreq>yearly</changefreq>
    <priority>0</priority>
  </url>
  <url>
    <loc>http://example.org/example7.html</loc>
    <lastmod>1970-01-01T00:00:00+00:00</lastmod>
    <changefreq>never</changefreq>
    <priority>1</priority>
  </url>
</urlset>

TAG;
		$this->assertEquals($expected, $xml);
	}
}
