<?php

declare(strict_types=1);

namespace asmaru\sitemap;

use DateTime;
use DOMDocument;
use DOMElement;

/**
 * Class SiteMapBuilder
 *
 * @package asmaru\sitemap
 */
class SiteMapBuilder {

	/**
	 * @var UrlElement[]
	 */
	private array $elements = [];

	/**
	 * @param string $location
	 * @param string|null $lastModified
	 * @param Frequency|null $changeFrequency
	 * @param float|null $priority
	 * @return SiteMapBuilder
	 */
	public function add(string $location, string $lastModified = null, Frequency $changeFrequency = null, float $priority = null): SiteMapBuilder {
		$urlElement = new UrlElement();
		$urlElement->setLocation($location);
		if ($lastModified !== null) {
			$lastModified = $lastModified instanceof DateTime ? $lastModified : DateTime::createFromFormat('U', $lastModified);
			$urlElement->setLastModified($lastModified);
		}
		if ($changeFrequency !== null) {
			$urlElement->setChangeFrequency($changeFrequency);
		}
		if ($priority !== null) {
			$priority = max(0, min(1, $priority));
			$urlElement->setPriority($priority);
		}
		$this->elements[] = $urlElement;
		return $this;
	}

	/**
	 * @return string
	 */
	public function __toString(): string {
		$doc = new DOMDocument();
		$doc->formatOutput = true;

		/** @var DOMElement $urlset */
		$urlset = $doc->appendChild($doc->createElement('urlset'));
		$urlset->setAttribute('xmlns', 'http://www.sitemaps.org/schemas/sitemap/0.9');

		foreach ($this->elements as $url) {
			$urlElement = $urlset->appendChild($doc->createElement('url'));

			$loc = $urlElement->appendChild($doc->createElement('loc'));
			$loc->appendChild($doc->createTextNode($url->getLocation()));

			if ($url->getLastModified() !== null) {
				$lastmod = $urlElement->appendChild($doc->createElement('lastmod'));
				$lastmod->appendChild($doc->createTextNode($url->getLastModified()->format('c')));
			}

			if ($url->getChangeFrequency() !== null) {
				$changefreq = $urlElement->appendChild($doc->createElement('changefreq'));
				$changefreq->appendChild($doc->createTextNode($url->getChangeFrequency()->__toString()));
			}

			if ($url->getPriority() !== null) {
				$priority = $urlElement->appendChild($doc->createElement('priority'));
				$priority->appendChild($doc->createTextNode(strval($url->getPriority())));
			}
		}
		return $doc->saveXML();
	}
}