<?php

declare(strict_types=1);

namespace asmaru\sitemap;

/**
 * Class Frequency
 *
 * @package asmaru\sitemap
 */
class Frequency {

	/**
	 * @var string
	 */
	private string $value;

	/**
	 * Frequency constructor.
	 *
	 * @param string $value
	 */
	public function __construct(string $value) {
		$this->value = $value;
	}

	/**
	 * @return Frequency
	 */
	public static function always(): Frequency {
		return new static('always');
	}

	/**
	 * @return Frequency
	 */
	public static function hourly(): Frequency {
		return new static('hourly');
	}

	/**
	 * @return Frequency
	 */
	public static function daily(): Frequency {
		return new static('daily');
	}

	/**
	 * @return Frequency
	 */
	public static function weekly(): Frequency {
		return new static('weekly');
	}

	/**
	 * @return Frequency
	 */
	public static function monthly(): Frequency {
		return new static('monthly');
	}

	/**
	 * @return Frequency
	 */
	public static function yearly(): Frequency {
		return new static('yearly');
	}

	/**
	 * @return Frequency
	 */
	public static function never(): Frequency {
		return new static('never');
	}

	/**
	 * @return string
	 */
	public function __toString(): string {
		return $this->value;
	}
}