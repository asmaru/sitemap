<?php

declare(strict_types=1);

namespace asmaru\sitemap;

use DateTime;

/**
 * Class UrlElement
 *
 * @package asmaru\sitemap
 */
class UrlElement {

	/**
	 * @var string
	 */
	private string $location;

	/**
	 * @var DateTime
	 */
	private DateTime $lastModified;

	/**
	 * @var Frequency
	 */
	private Frequency $changeFrequency;

	/**
	 * @var float
	 */
	private float $priority = 1;

	public function __construct() {
		$this->location = '';
		$this->lastModified = new DateTime();
		$this->changeFrequency = Frequency::always();
	}

	/**
	 * @return string
	 */
	public function getLocation(): string {
		return $this->location;
	}

	/**
	 * @param string $location
	 */
	public function setLocation(string $location) {
		$this->location = $location;
	}

	/**
	 * @return DateTime
	 */
	public function getLastModified(): DateTime {
		return $this->lastModified;
	}

	/**
	 * @param DateTime $lastModified
	 */
	public function setLastModified(DateTime $lastModified) {
		$this->lastModified = $lastModified;
	}

	/**
	 * @return Frequency
	 */
	public function getChangeFrequency(): Frequency {
		return $this->changeFrequency;
	}

	/**
	 * @param Frequency $changeFrequency
	 */
	public function setChangeFrequency(Frequency $changeFrequency) {
		$this->changeFrequency = $changeFrequency;
	}

	/**
	 * @return float
	 */
	public function getPriority(): float {
		return $this->priority;
	}

	/**
	 * @param float $priority
	 */
	public function setPriority(float $priority) {
		$this->priority = $priority;
	}
}